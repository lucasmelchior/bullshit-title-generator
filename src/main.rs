/*
 Copyright © 2022 Drew Short <warrick@sothr.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#[macro_use]
extern crate lazy_static;

use clap::Parser;
use rand::seq::SliceRandom;
use serde::{Deserialize, Serialize};
use serde_yaml;

lazy_static! {
    static ref UNKNOWN: String = String::from("UNKNOWN");
}

#[derive(Debug, Serialize, Deserialize)]
struct TitleParts {
    prefix: Vec<String>,
    topic: Vec<String>,
    suffix: Vec<String>,
}

#[derive(Parser, Debug)]
#[clap(version, about, long_about = None)]
struct Args {
    #[clap(
        short,
        long,
        help = "The number of titles to generate",
        default_value_t = 1
    )]
    count: u8,
}

fn main() {
    let titles = include_bytes!("static/titles.yaml");
    let title_parts: TitleParts = match serde_yaml::from_slice(titles) {
        Ok(result) => result,
        Err(_) => {
            panic!("Failed to load titles: {:?}", titles);
        }
    };

    let mut random = rand::thread_rng();

    let args = Args::parse();
    for _ in 0..args.count {
        let prefix = title_parts.prefix.choose(&mut random).unwrap_or(&UNKNOWN);
        let topic = title_parts.topic.choose(&mut random).unwrap_or(&UNKNOWN);
        let suffix = title_parts.suffix.choose(&mut random).unwrap_or(&UNKNOWN);
        println!("{} {} {}", prefix, topic, suffix)
    }
}
